#include <Arduino.h>

static const uint8_t LED_PIN = 10;
static const uint8_t BTN_PIN = 2;

void setup()
{
    Serial.begin(9600);

    pinMode(LED_PIN, OUTPUT);
    pinMode(BTN_PIN, INPUT_PULLUP);
}

void loop()
{
    static uint32_t last_millis = 0;
    static bool button_pressed = false;
    static bool led_blinking = false;
    static bool led_state = false;

    if (millis() - last_millis < 100)
        return;
    last_millis = millis();

    bool pin_value = digitalRead(BTN_PIN);

    if (!pin_value && !button_pressed)
    {
        Serial.println("Button pressed");
        button_pressed = true;

        led_blinking = !led_blinking;

        if (!led_blinking)
        {
            led_state = false;
            digitalWrite(LED_PIN, 0);
        }

        Serial.print("LED blinking: ");
        Serial.println(led_blinking ? "Yes" : "No");
    }
    else if (pin_value && button_pressed)
    {
        Serial.println("Button released");
        button_pressed = false;
    }

    if (led_blinking)
    {
        led_state = !led_state;
        digitalWrite(LED_PIN, led_state ? 1 : 0);
    }
}