#include <Arduino.h>
#include <U8glib.h>

U8GLIB_SSD1306_128X64* g_oled = nullptr;

/*
    layout:
        width: 40 (10 * 4) + margin
        height: 64 (16 * 4) + margin
*/

#define BOARD_WIDTH             13
#define BOARD_HEIGHT            16
#define BOARD_RENDER_OFFSET_X   2
#define BOARD_RENDER_OFFSET_Y   2

static bool g_board[BOARD_HEIGHT][BOARD_WIDTH] = { 0 };

void draw()
{
    g_oled->drawFrame(0, 0, 83, 63);

    // Draw the board
    uint8_t x = BOARD_RENDER_OFFSET_X;
    for (uint8_t i = 0; i < BOARD_HEIGHT; ++i)
    {
        uint8_t y = 63 - BOARD_RENDER_OFFSET_Y - 4;
        for (uint8_t j = 0; j < BOARD_WIDTH; ++j)
        {
            if (g_board[i][j])
            {
                g_oled->drawFrame(x, y, 4, 4);
                y -= 5;
            }
        }
        x += 5;
    }
}

void setup()
{
    static U8GLIB_SSD1306_128X64 s_oled;
    g_oled = &s_oled;
    s_oled.setContrast(0);

    for (uint8_t i = 0; i < BOARD_WIDTH * BOARD_HEIGHT; ++i)
        g_board[0][i] = i % 5 == 0;
}

void loop()
{
    g_oled->firstPage();
    do {
        draw();
    } while (g_oled->nextPage());
}