#include <Arduino.h>
#include <U8glib.h>
#include <stdio.h>

U8GLIB_SSD1306_128X64* g_oled = nullptr;

char g_analogValue[30] = { 0 };
char g_buttonCode[30] = { 0 };

// Update status messages on the screen
void draw()
{
    // Load the built-in font helvB (8 px).
    // For more fonts, see https://github.com/olikraus/u8glib/wiki/fontsize
    g_oled->setFont(u8g_font_helvB08);

    // Draw the status messages. 
    // For more info, see https://github.com/olikraus/u8glib/wiki
    g_oled->drawStr(0, 8, g_analogValue);
    g_oled->drawStr(0, 17, g_buttonCode);
}

// Read the ADC to determine which button is being pressed
uint8_t readButton()
{
    // Read the analog value from the A0 port's ADC.
    // This value is a 10 bit number depending on the 
    // ADC setup, which has a 10 bit resolution by default.
    int value = analogRead(A0);

    sprintf(g_analogValue, "analog: %d", value);

    // These analog values should be measured for each setup
    // since they can vary due to the properties of the circuit,
    // resistors, breadboard etc.
    if (value > 370)
        return 4;
    else if (value > 270)
        return 3;
    else if (value > 220)
        return 2;
    else if (value > 100)
        return 1;

    return 0;
}

void setup()
{
    // Use the SSD1306 driver in I2C mode
    U8GLIB_SSD1306_128X64 s_oled;
    g_oled = &s_oled;

    // Lower the contrast to protect the display
    g_oled->setContrast(0);

    // Setup the A0 pin as analog input
    pinMode(A0, INPUT);
}

void loop()
{
    static uint32_t lastMillis = 0;

    if (millis() - lastMillis > 50)
    {
        lastMillis = millis();

        auto button = readButton();
        sprintf(g_buttonCode, "button: %u", button);

        // This is the essential part of the u8glib's picture loop
        g_oled->firstPage();
        do
        {
            draw();
        }
        while (g_oled->nextPage());
    }
}