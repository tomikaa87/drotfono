#include <Arduino.h>

void setup()
{
    Serial.begin(9600);
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
    static uint32_t last_millis = 0u;
    static bool led_on = false;

    if (millis() - last_millis >= 1000u)
    {
        last_millis = millis();

        digitalWrite(LED_BUILTIN, led_on ? HIGH : LOW);
        
        Serial.print("LED: ");
        Serial.println(led_on ? "ON" : "OFF");
        
        led_on = !led_on;
    }
}