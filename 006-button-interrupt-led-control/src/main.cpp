#include <Arduino.h>

static const uint8_t LED_PIN = 10;
static const uint8_t BTN_PIN = 2;

static bool button_pressed = false;
static bool led_pulsating = false;

void buttonInterruptHandler()
{
    static uint32_t last_state_change_millis = 0;

    // Read the button
    bool pin_value = digitalRead(BTN_PIN) > 0;

    // If pin_value is false, the button is pressed because
    // it connects the input pin to GND. If the button is released
    // the pin's value is true, because the internal pull-up
    // resistor connects it to VDD (+5V).

    if (!pin_value && !button_pressed)
    {
        // Primitive software debouncing, avoid rapid state changes
        if (millis() - last_state_change_millis < 200)
            return;
        last_state_change_millis = millis();

        Serial.println("Button pressed");
        button_pressed = true;

        led_pulsating = !led_pulsating;
        Serial.print("LED pulsating: ");
        Serial.println(led_pulsating ? "Yes" : "No");
    }
    else if (pin_value && button_pressed)
    {
        Serial.println("Button released");
        button_pressed = false;
    }
}

void setup()
{
    Serial.begin(9600);

    // Setup LED's pin as output
    pinMode(LED_PIN, OUTPUT);

    // Setup button's pin as an input using the internal pull-up resistor
    pinMode(BTN_PIN, INPUT_PULLUP);

    // More on attachInterrupt: https://www.arduino.cc/en/Reference/AttachInterrupt
    attachInterrupt(digitalPinToInterrupt(BTN_PIN), buttonInterruptHandler, CHANGE);
}

void loop()
{
    static uint32_t last_millis = 0;
    static bool pwm_increasing = true;
    static uint8_t pwm_dc = 0;

    if (millis() - last_millis < 5 || !led_pulsating)
        return;
    last_millis = millis();

    if (pwm_increasing)
        ++pwm_dc;
    else
        --pwm_dc;

    if (pwm_dc == 255)
        pwm_increasing = false;
    else if (pwm_dc == 0)
        pwm_increasing = true;

    analogWrite(LED_PIN, pwm_dc);
}