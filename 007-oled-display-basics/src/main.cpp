#include <Arduino.h>
#include <Wire.h>

#include "oled.h"

Oled* g_oled = nullptr;

void drawTestPattern(uint8_t x, uint8_t y)
{
    g_oled->clear();

    g_oled->setPixel(0, 0, Oled::Color::White);
    g_oled->setPixel(0, 1, Oled::Color::White);
    g_oled->setPixel(1, 0, Oled::Color::White);
    
    g_oled->setPixel(126, 0, Oled::Color::White);
    g_oled->setPixel(127, 0, Oled::Color::White);
    g_oled->setPixel(127, 1, Oled::Color::White);
    
    g_oled->setPixel(0, 63, Oled::Color::White);
    g_oled->setPixel(1, 63, Oled::Color::White);
    g_oled->setPixel(0, 62, Oled::Color::White);
    
    g_oled->setPixel(127, 63, Oled::Color::White);
    g_oled->setPixel(126, 63, Oled::Color::White);
    g_oled->setPixel(127, 62, Oled::Color::White);

    for (uint8_t i = 2; i < 64; i += 3)
        g_oled->drawCircle(x, y, i, Oled::Color::White);

    g_oled->update();
}

void setup()
{
    Serial.begin(9600);
    Serial.println("*** OLED display demo ***");

    static Oled s_oled;
    g_oled = &s_oled;
}

void loop()
{
    static bool incX = true;
    static bool incY = true;
    
    static uint8_t x = 63;
    static uint8_t y = 31;

    drawTestPattern(x, y);

    if (incX)
        ++x;
    else
        --x;

    if (incY)
        ++y;
    else
        --y;

    if (x == 0)
        incX = true;
    else if (x == 127)
        incX = false;

    if (y == 0)
        incY = true;
    else if (y == 63)
        incY = false;
}