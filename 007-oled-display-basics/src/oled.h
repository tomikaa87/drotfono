#include <stdint.h>

class Oled
{
public:
    static const uint8_t Width = 128;
    static const uint8_t Height = 64;
    static const uint16_t BufferSize = Width * Height / 8;

    Oled();

    enum class AddressingMode : uint8_t
    {
        Horizontal = 0b00,
        Vertical = 0b01,
        Page = 0b10
    };

    enum class Color
    {
        Black,
        White,
        Inverse
    };

    void clear();
    void update();

    void setPixel(uint8_t x, uint8_t y, Color color);
    void drawCircle(uint8_t x0, uint8_t y0, uint8_t radius, Color color);

    void sendData(uint8_t* data, uint8_t length);

    void setAddressingMode(AddressingMode mode);
    void setColumnAddress(uint8_t start, uint8_t end);
    void setPageAddress(uint8_t start, uint8_t end);

    void fillPattern(uint8_t pattern);

private:
    static const uint8_t DeviceAddress = 0x3C;
    static const uint8_t DCFlag = 0x40;

    enum class Command : uint8_t 
    {
        SetContrast = 0x81u,
        SetEntireDisplayOnResume = 0xA4u,
        SetEntireDisplayOn = 0xA5u,
        SetNormalDisplay = 0xA6u,
        SetInverseDisplay = 0xA7u,
        SetDisplayOff = 0xAEu,
        SetDisplayOn = 0xAFu,
        SetDisplayOffset = 0xD3u,
        SetCOMPins = 0xDAu,
        SetVCOMDeselect = 0xDBu,
        SetDisplayClockDiv = 0xD5u,
        SetPreCharge = 0xD9u,
        SetMultiplex = 0xA8u,
        SetLowColumn = 0x00u,
        SetHighColumn = 0x10u,
        SetStartLine = 0x40u,
        SetAddressingMode = 0x20u,
        SetColumnAddr = 0x21u,
        SetPageAddr = 0x22u,
        SetComScanInc = 0xC0u,
        SetCOMScanDec = 0xC8u,
        SetSegmentRemap = 0xA0u,
        SetChargePump = 0x8Du,
        SetExternalVcc = 0x01u,
        SetSwitchCapVcc = 0x02u,
        ActivateScroll = 0x2Fu,
        DeactivateScroll = 0x2Eu,
        SetVerticalScrollArea = 0xA3u,
        RightHorizonalScroll = 0x26u,
        LeftHorizontalScroll = 0x27u,
        VertialAndRightHorizonalScroll = 0x29u,
        VertialAndLeftHorizonalScroll = 0x2Au,
        SetPageStartAddr = 0xB0u
    };

    // Communication primitives
    void sendCommand(Command command, uint8_t flags = 0);
    void sendCommandArg(Command command, uint8_t argument);

    uint8_t m_buffer[BufferSize] = { 0 };
};