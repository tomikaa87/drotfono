#include "oled.h"

#include <Arduino.h>
#include <avr/pgmspace.h>
#include <Wire.h>

//#define DEBUG

Oled::Oled()
{
    Serial.println("OLED - initializing");
    
    Wire.begin();
    Wire.setClock(850000);

    // Setup the OLED driver IC
    sendCommand(Command::SetDisplayOff);
    sendCommandArg(Command::SetDisplayClockDiv, 0x80);
    sendCommandArg(Command::SetMultiplex, Height - 1);
    sendCommandArg(Command::SetDisplayOffset, 0x00);
    sendCommand(Command::SetStartLine);
    sendCommandArg(Command::SetChargePump, 0x14);
    sendCommand(Command::SetSegmentRemap, 0x01);
    sendCommand(Command::SetCOMScanDec);
    sendCommandArg(Command::SetCOMPins, 0x12);
    sendCommandArg(Command::SetContrast, 0x20);
    sendCommandArg(Command::SetVCOMDeselect, 0x10);
    sendCommand(Command::SetEntireDisplayOnResume);
    sendCommand(Command::SetNormalDisplay);
    sendCommand(Command::DeactivateScroll);
    sendCommand(Command::SetDisplayOn);

    clear();
    update();
}

void Oled::clear()
{
    memset(m_buffer, 0, BufferSize);
}

void Oled::setAddressingMode(AddressingMode mode)
{
    sendCommandArg(Command::SetAddressingMode, static_cast<uint8_t>(AddressingMode::Horizontal));
}

void Oled::setColumnAddress(uint8_t start, uint8_t end)
{
    sendCommand(Command::SetColumnAddr);
    sendCommand(static_cast<Command>(start));
    sendCommand(static_cast<Command>(end));
}

void Oled::setPageAddress(uint8_t start, uint8_t end)
{
    sendCommand(Command::SetPageAddr);
    sendCommand(static_cast<Command>(start));
    sendCommand(static_cast<Command>(end));
}

void Oled::fillPattern(uint8_t pattern)
{
    memset(m_buffer, pattern, BufferSize);
}

void Oled::update()
{
    setAddressingMode(AddressingMode::Horizontal);
    setColumnAddress(0, Width - 1);
    setPageAddress(0, Height / 8 - 1);

    const uint8_t* buffer = m_buffer;

    // Send pattern in 16 byte chunks
    for (uint16_t i = 0; i < BufferSize; i += 16)
    {
        Wire.beginTransmission(DeviceAddress);

        Wire.write(DCFlag);
        Wire.write(buffer, 16);
        buffer += 16;

        Wire.endTransmission();
    }
}

void Oled::setPixel(uint8_t x, uint8_t y, Color color)
{
    if (x >= Width || y >= Height)
        return;

    uint16_t address = x + (y / 8) * Width;
    uint8_t mask = 1 << (y & 7);

    switch (color)
    {
    case Color::White:
        m_buffer[address] |= mask;
        break;

    case Color::Black:
        m_buffer[address] &= ~mask;
        break;

    case Color::Inverse:
        m_buffer[address] ^= mask;
        break;
    }
}

void Oled::drawCircle(uint8_t x0, uint8_t y0, uint8_t radius, Color color)
{
    // Circle drawing algorithm from https://en.wikipedia.org/wiki/Midpoint_circle_algorithm

    int16_t x = radius;
    int16_t y = 0;
    int16_t err = 0;

    while (x >= y)
    {
        setPixel(x0 + x, y0 + y, color);
        setPixel(x0 + y, y0 + x, color);
        setPixel(x0 - y, y0 + x, color);
        setPixel(x0 - x, y0 + y, color);
        setPixel(x0 - x, y0 - y, color);
        setPixel(x0 - y, y0 - x, color);
        setPixel(x0 + y, y0 - x, color);
        setPixel(x0 + x, y0 - y, color);

        if (err <= 0)
        {
            y += 1;
            err += 2 * y + 1;
        }
        else
        {
            x -= 1;
            err -= 2 * x + 1;
        }
    }
}

void Oled::sendCommand(Command command, uint8_t flags)
{
#ifdef DEBUG
    Serial.print("OLED - sendCommand: ");
    Serial.print((unsigned)command, 16);
#endif

    uint8_t commandByte = static_cast<uint8_t>(command) | flags;

    // First byte selects command/data mode (DC flag, 0: command)
    uint8_t data[2] = { 0, commandByte };

    Wire.beginTransmission(DeviceAddress);

#ifdef DEBUG
    if (Wire.write(data, sizeof(data)))
        Serial.println(" - OK");
    else
        Serial.println(" - ERROR");
#else
    Wire.write(data, sizeof(data));
#endif

    Wire.endTransmission();
}

void Oled::sendCommandArg(Command command, uint8_t argument)
{
    sendCommand(command);
    sendCommand(static_cast<Command>(argument));
}

void Oled::sendData(uint8_t* data, uint8_t length)
{
    Wire.beginTransmission(DeviceAddress);
    Wire.write(DCFlag);
    Wire.write(data, length);
    Wire.endTransmission();
}
