#include <Arduino.h>

void setup()
{
    Serial.begin(9600);
    Serial.println("LED Control: 1 - turn on, 0 - turn off");

    pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
    if (Serial.available())
    {
        auto c = char(Serial.read());
        bool led_on = false;

        if (c == '1')
            led_on = true;
        else if (c == '0')
            led_on = false;
        else
        {
            Serial.println("Invalid command");
            return;
        }

        digitalWrite(LED_BUILTIN, led_on ? HIGH : LOW);

        Serial.print("LED: ");
        Serial.println(led_on ? "ON" : "OFF");
    }
}