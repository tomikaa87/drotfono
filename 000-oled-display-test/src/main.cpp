#include <Arduino.h>
#include <U8glib.h>

U8GLIB_SSD1306_ADAFRUIT_128X64 u8g{ U8G_I2C_OPT_DEV_0 };

void draw()
{
    u8g.drawFrame(0, 0, 128, 64);
    u8g.drawFilledEllipse(63, 31, 10, 10);
    u8g.drawLine(0, 0, 127, 63);
    u8g.drawLine(0, 63, 127, 0);
}

void setup()
{
    u8g.begin();
}

void loop()
{
    static uint32_t last_millis = 0;

    // Run "picture loop"
    if (millis() - last_millis >= 100)
    {
        last_millis = millis();
        
        u8g.firstPage();
        do {
            draw();
        } while (u8g.nextPage());
    }
}