#include <stdint.h>
#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Servo.h>

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);
Servo servo;

void setup()
{
    lcd.begin(16, 4);
    lcd.print("Angle:");

    servo.attach(9);
}

void setServoAngle(uint16_t angle)
{
    lcd.setCursor(7, 0);
    lcd.print("   ");
    lcd.setCursor(7, 0);
    lcd.print(angle);

    servo.write(angle);
}

void loop()
{
    setServoAngle(0);
    delay(1000);
    setServoAngle(360);
    delay(1000);
}