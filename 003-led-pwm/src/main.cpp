#include <Arduino.h>

static const uint8_t LED_PIN = 10;

void setup()
{
    Serial.begin(9600);
    Serial.println("LED brightness control with PWM");

    pinMode(LED_PIN, OUTPUT);
}

void loop()
{
    static uint32_t last_millis = 0;
    static uint8_t led_pwm = 0;
    static bool increment = true;

    if (millis() - last_millis >= 10)
    {
        last_millis = millis();

        if (increment)
            ++led_pwm;
        else
            --led_pwm;

        if (led_pwm == 0)
            increment = true;
        else if (led_pwm == 255)
            increment = false;

        Serial.print(increment ? "Incrementing" : "Decrementing");
        Serial.print(". New value: ");
        Serial.println(led_pwm);

        analogWrite(LED_PIN, led_pwm);
    }
}