# Welcome to DrótFonó
---

You can find the source codes for all the projects in the numbered folders, like `001-blink-serial`.  
Also there are supplemental documentations in the `doc` folder, including some slides with useful basic circuits in the `basics` folder.  
Schematics can be found in the `schematics` folder in PNG and CadSoft EAGLE format.

## We use Arduino Nano for all the projects
---
![Arduino Nano pinout.png](https://bitbucket.org/repo/k8koa6/images/3756373819-Arduino%20Nano%20pinout.png)