#include <Arduino.h>
#include <string.h>

static const uint8_t LED_PIN = 10;

void set_pwm_value(const char* value_str)
{
    int value = strtol(value_str, nullptr, 10);

    if (value < 0 || value > 255)
    {
        Serial.println("Error: value must be between 0 and 255");
        return;
    }

    Serial.print("New PWM value: ");
    Serial.println(value);

    analogWrite(LED_PIN, value);
}

void setup()
{
    Serial.begin(9600);
    Serial.println("Enter new PWM value (0-255) and press Enter");

    pinMode(LED_PIN, OUTPUT);
    pinMode(LED_BUILTIN, OUTPUT);
}

void loop()
{
    static char buffer[4] = { 0 };
    static uint8_t buffer_index = 0;

    if (Serial.available())
    {
        auto c = char(Serial.read());

        if (c == '\r')
            return;

        if (c == '\n' || buffer_index == sizeof(buffer) - 1)
        {
            if (buffer_index == 0)
                return;

            set_pwm_value(buffer);

            buffer_index = 0;
            memset(buffer, 0, sizeof(buffer));

            digitalWrite(LED_BUILTIN, LOW);
        }
        else
        {
            buffer[buffer_index++] = c;

            digitalWrite(LED_BUILTIN, HIGH);
        }
    }
}